package com.example.demo.application.handler;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;


@Component
public class WordHandlerFunction {

    public Mono<ServerResponse> getWord(ServerRequest serverRequest) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just("one").log(), String.class);

    }

    public Mono<ServerResponse> getWords(ServerRequest serverRequest) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(Flux.just("one", "two", "three", "four", "five").log(), String.class);
    }


    public Mono<ServerResponse> getNoneBlockWords(ServerRequest serverRequest) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .body(Flux.just("one", "two", "three", "four", "five").delayElements(Duration.ofSeconds(1)).log(), String.class);
    }

}

