package com.example.demo.application.restcontroller;

import jdk.nashorn.internal.objects.annotations.Getter;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@RequestMapping
public class NumberRestController {

    @GetMapping("number")
    public Mono<Integer> getInteger(){
        return Mono.just(1).delayElement(Duration.ofSeconds(2)).log();
    }

    @GetMapping("numbers")
    public Flux<Integer> getIntegers() {
        return Flux.just(1,2,3,4).log();
    }

    @GetMapping(value = "noneblock/numbers", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Integer> getIntegersNoneBlock() {
        return Flux.just(1,2,3,4).delayElements(Duration.ofSeconds(1)).log();
    }

}
