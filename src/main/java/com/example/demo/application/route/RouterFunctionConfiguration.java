package com.example.demo.application.route;

import com.example.demo.application.handler.WordHandlerFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

@Configuration
public class RouterFunctionConfiguration {


    @Bean
    public RouterFunction<ServerResponse> route(WordHandlerFunction handler){
        return RouterFunctions
                .route(GET("word").and(accept(MediaType.APPLICATION_JSON)), handler::getWord)
                .andRoute(GET("words").and(accept(MediaType.APPLICATION_JSON)), handler::getWords)
                .andRoute(GET("noneblock/words").and(accept(MediaType.APPLICATION_JSON)), handler::getNoneBlockWords);
    }

}
